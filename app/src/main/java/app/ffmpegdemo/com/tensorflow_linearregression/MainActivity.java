package app.ffmpegdemo.com.tensorflow_linearregression;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

public class MainActivity extends AppCompatActivity {


    private static final String MODEL_NAME = "file:///android_asset/optimized_frozen_linear_regression.pb";
    private static final String INPUT_NODE = "x";
    private static final String OUTPUT_NODE = "y_output";
    private static final int[] INPUT_SHAPE = {1, 1}; // becuase we are input only 1 column hence (1 row 1 column)
    private TensorFlowInferenceInterface inferenceInterface;

    EditText editText;
    TextView textView;
    Button button;

    static {
        System.loadLibrary("tensorflow_inference");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.edit_text);
        textView = findViewById(R.id.text_view);
        button = findViewById(R.id.button);

        inferenceInterface = new TensorFlowInferenceInterface();
        inferenceInterface.initializeTensorFlow(getAssets(), MODEL_NAME);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float input = Float.parseFloat(editText.getText().toString());
                String result = performInference(input);
                textView.setText(result );
            }
        });

    }

    private String performInference(float input)
    {

        /*
         Since we are only dealing with floats in our models hence we asre using float here

         */
        float [] floatArray = {input};
        inferenceInterface.fillNodeFloat(INPUT_NODE,INPUT_SHAPE,floatArray);
        inferenceInterface.runInference(new String[] {OUTPUT_NODE});
        float[] results = {0.0f};
        inferenceInterface.readNodeFloat(OUTPUT_NODE,results);
        String finalResult = String.valueOf(results[0]);
        return finalResult;
    }
}
